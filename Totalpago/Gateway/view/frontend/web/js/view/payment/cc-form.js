/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
        [
            'underscore',
            'Magento_Checkout/js/view/payment/default'

        ],
        function (_, Component,$t) {
            'use strict';

            return Component.extend({
                

                initObservable: function () {
                    this._super();
                    this.referenceDate = this.getDateCurrent();
                    this.observe([
                                'bankType',
                                'referenceNumber',
                                'referenceDate',
                                'cedulaNumber',
                                'documentType',
                                'fullname',
                                'telephone',
                                'email'
                            ]);

                    return this;
                },

                /**
                 * Init component
                 */
                initialize: function () {
                    var self = this;

                    this._super();

                },

                /**
                 * Get code
                 * @returns {String}
                 */
                getCode: function () {
                    return 'totalpago';
                },

                /**
                 * Get data
                 * @returns {Object}
                 */

                 

                getData: function () {

                    
                    return {
                        'method': this.getCode(),
                        'additional_data': {
                            'bank_type':this.bankType(),
                            'reference_number': this.referenceNumber(),
                            'reference_date':this.referenceDate(),
                            'document_type':this.documentType(),
                            'cedula_number': this.cedulaNumber(),
                            'fullname': this.fullname(),
                            'telephone': this.telephone(),
                            'email': this.email()

                        }
                    };
                },


                getBanks: function () {
                    return window.checkoutConfig.payment.ccform.banks[this.getCode()];
                },

                getBanksValues: function () {

                    return _.map(this.getBanks(), function (value, key) {
                        //console.log(value);
                        return {
                            'idbancoTransf': value.idbancoTransf,
                            'nmbancoTransf': value.nmbancoTransf,
                            'nmTitularCuentaTransf':value.nmTitularCuentaTransf,
                            //'nmbancoTransf':value.nmbancoTransf,
                            'nuCuentaBancoTransf':value.nuCuentaBancoTransf,
                            'rifTitularCuentaTransf':value.rifTitularCuentaTransf
                        };
                    });
                },

                getDocuments: function () {
                    return window.checkoutConfig.payment.ccform.documentTypes[this.getCode()];
                },

                getDocumentValues: function () {

                    return _.map(this.getDocuments(), function (value, key) {
                        //console.log(value);
                        return {
                            'clave': value.clave,
                            'valor': value.valor
                        };
                    });
                },

                getDateCurrent: function(){
                    return window.checkoutConfig.payment.ccform.dateCurrent[this.getCode()];
                },

                isShowLegend: function () {
                    return false;
                },


                getCcTypeTitleByCode: function (code) {
                    var title = '',
                            keyValue = 'value',
                            keyType = 'type';

                    _.each(this.getCcAvailableTypesValues(), function (value) {
                        if (value[keyValue] === code) {
                            title = value[keyType];
                        }
                    });

                    return title;
                },

            });
        }
);
