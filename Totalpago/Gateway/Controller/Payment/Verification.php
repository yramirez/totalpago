<?php
namespace Totalpago\Gateway\Controller\Payment;

use Totalpago\Gateway\Model as Service;
use Magento\Framework\Controller\ResultFactory;
use Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Sales\Model\Service\InvoiceService;
use Magento\Framework\DB\Transaction;

class Verification extends \Magento\Framework\App\Action\Action{

    protected $resultPageFactory;


    protected $transactionBuilder;


    protected $_orderRepository;
 

    protected $_invoiceService;

    protected $_transaction;
    
    protected $_invoiceSender;

    protected $_objectManager;

    protected $_cc;

    protected $_checkoutSession;

     protected $_registry;
     protected $_scopeConfig;
     protected $_logger;

     protected $messageManager;

     protected $_errorCodes = array("APROBADO"=>"APROBADO","RECHAZADO"=>"RECHAZADO","EN PROCESO"=>"EN PROCESO","PENDIENTE"=>"PENDIENTE");

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Sales\Model\Service\InvoiceService $invoiceService,
        \Magento\Framework\DB\Transaction $transaction,
            \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Psr\Log\LoggerInterface $logger,
         Service\TotalpagoService $service
    ) {
    
        parent::__construct($context);
        $this->_transactionBuilder = $transactionBuilder;
        $this->_orderRepository = $orderRepository;
        $this->_invoiceService = $invoiceService;
        $this->_transaction = $transaction;
        $this->_objectManager = $objectManager;
        $this->_registry = $registry;
        $this->_checkoutSession = $checkoutSession;
        $this->_scopeConfig = $scopeConfig;
        $this->_logger = $logger;

        $this->messageManager = $context->getMessageManager();

        $this->_service = $service;

    }

    /**
     * Show payment page
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        $params = array("idPago"=>$this->getRequest()->getParam("idPago"));
        $response = $this->_service->verify($params);


        if ($response->Mensaje==="APROBADO") {
        
            $orderId = $this->_checkoutSession->getLastRealOrderId();

            $order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($orderId);

            $paymentData = $this->getRequest()->getParams();
            $this->createInvoice($order);
            $this->createTransaction($order,array($response->Mensaje));
            
            return $resultRedirect->setData($response);
            //return $resultRedirect->setPath('checkout/onepage/success/');
  
        }else{

          $level = 'ERROR';
          $this->_logger->log($level,'TOTALPAGO',$this->getRequest()->getParams()); 
            $orderId = $this->_checkoutSession->getLastRealOrderId();
            $order = $this->_objectManager->create('\Magento\Sales\Model\Order')->loadByIncrementId($orderId);
            if($order->canCancel()){
                $order->cancel()->save();
            }
            $this->_objectManager->create('\Magento\Checkout\Model\Session')->clearQuote();
            
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_JSON);


            $message = "<b>Estimado cliente la transaction no pudo ser procesada exitosamente.</b>\n";

            if($response->Mensaje!==null){
              $message .= "<b>El motivo de la transacción fallida es por: ".$this->_getErrorMessage($response->Mensaje)."</b>";

              $order->addStatusHistoryComment(__($message))->setIsCustomerNotified(true)->save();
              
              $level = 'ERROR';
              $this->_logger->log($level,'TOTALPAGO',$message); 
            }
            
            $this->_checkoutSession->setErrorMessage($message);

            //$this->messageManager->addErrorMessage($message);
            return  $resultRedirect->setData($response);
            //return $resultRedirect->setPath('checkout/onepage/failure');
         }
    }


    private function createTransaction($order = null, $paymentData = array())
    {
        try {
            //get payment object from order object
            $payment = $order->getPayment();
            $transId=substr(rand() * 900000 + 100000, 0,6);
            $payment->setLastTransId($transId);
            $payment->setTransactionId($transId);
            $payment->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            );
            $formatedPrice = $order->getBaseCurrency()->formatTxt(
                $order->getGrandTotal()
            );
 
            $message = __('Capture Pay');
            //get the object of builder class
            $trans = $this->_transactionBuilder;
            $transaction = $trans->setPayment($payment)
            ->setOrder($order)
            ->setTransactionId($transId)
            ->setAdditionalInformation(
                [\Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS => (array) $paymentData]
            )
            ->setFailSafe(true)
            //build method creates the transaction and returns the object
            ->build(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_CAPTURE);
 
            $payment->addTransactionCommentsToOrder(
                $transaction,
                $message
            );
            $payment->setParentTransactionId(null);
            $payment->save();
            $order->save();
 
            return  $transaction->save()->getTransactionId();
        } catch (Exception $e) {
            //log errors here
        }
    }

    private function createInvoice($order){


        if($order->canInvoice()) {
            $order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING, true);
            $order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
            $order->addStatusToHistory($order->getStatus(), 'Order processed successfully with reference');
            $order->save();
            
            $invoice = $this->_invoiceService->prepareInvoice($order);
            $invoice->register()->capture()->pay();
            $invoice->save();
            $transactionSave = $this->_transaction->addObject(
                $invoice
            )->addObject(
                $invoice->getOrder()
            );
            $transactionSave->save();
            
            $order->addStatusHistoryComment(
                __('Notified customer about invoice #%1.', $invoice->getId())
            )->setIsCustomerNotified(true)->save();
        }
    }

    private function _getErrorMessage($code){

        if(array_key_exists($code, $this->_errorCodes)){

          return $this->_errorCodes[$code];

        }else{
           return null;
        }

    }
}