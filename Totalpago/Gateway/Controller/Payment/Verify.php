<?php

namespace Totalpago\Gateway\Controller\Payment;

//use Adverweb\Cardnet\Model\PaymentMethod;

/**
 * Class Request
 *
 * @package Adverweb\Cardnet\Controller\Payment
 */
class Verify extends \Magento\Framework\App\Action\Action
{
    
     /** @var  \Magento\Framework\View\Result\Page */
    protected $resultPageFactory;

    /**
     * @var \Adverweb\Cardnet\Model\PaymentMethod
     */
    


    /**
     * Checkout constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;

    }

    /**
     * Show payment page
     * @return \Magento\Framework\View\Result\PageFactory
     */
    public function execute()
    {
        

        $resultPage = $this->resultPageFactory->create();
        
        //$resultPage->getLayout()->getBlock('totalpago.payment.redirect')->setOrderId($this->lastRealOrderId());
        //$resultPage->getLayout()->getBlock('totalpago.payment.redirect')->setOrder($this->loadOrder());
         
        return $resultPage;
    }

     private function order()
    {
        return $this->_objectManager->create('Magento\Sales\Model\Order')->load($this->session()->order_id);
    }

     private function lastRealOrderId()
    {
        return $this->_objectManager->create('\Magento\Checkout\Model\Session')->getLastRealOrder()->getId();
    }

     private function loadOrder()
    {
        return $this->_objectManager->create('Magento\Sales\Model\Order')->load($this->lastRealOrderId());
    }
    
}
