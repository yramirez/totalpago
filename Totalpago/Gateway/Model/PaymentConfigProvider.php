<?php

namespace Totalpago\Gateway\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Framework\View\Asset\Source;


class PaymentConfigProvider implements ConfigProviderInterface
{


    const PAYMENT_METHOD_CODE = \Totalpago\Gateway\Model\Totalpago::METHOD_CODE;

    /**
     * @var
     */
    private $method;

    protected $_ccConfig;

    protected $_date;

    /**
     * @var array
     */
    private $icons = [];

    /**
     * @var \Magento\Framework\View\Asset\Source
     */
    protected $assetSource;

    protected $timeoutTime = '5000';

    /**
     * PaymentConfigProvider constructor.
     * @param PaymentHelper $helper
     */
    public function __construct(PaymentHelper $helper,
        \Magento\Payment\Model\CcConfig $ccConfig,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        Source $assetSource,
        TotalpagoService $totalPagoService,
        array $methodCodes = [])
    {
        $this->method = $helper->getMethodInstance(self::PAYMENT_METHOD_CODE);
        $this->_ccConfig = $ccConfig;
        $this->_date = $date;
        $this->assetSource = $assetSource;

        $this->_totalPagoService = $totalPagoService;
    }

    /**
     * Get payment config
     *
     * @return array
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                self::PAYMENT_METHOD_CODE => [
                    'isPagar'   => true,
                    'checkout'   => [                      
                        'redirect' => $this->method->getRedirectCheckoutPaymentUrl()
                    ],

                    
                ],
                'ccform'=>[
                    'hasVerification'=>[ self::PAYMENT_METHOD_CODE=>true],
                    'cvvImageUrl' => [self::PAYMENT_METHOD_CODE => $this->getCvvImageUrl()],
                    'availableTypes'=>[self::PAYMENT_METHOD_CODE => $this->getCcAvailableTypes()],
                    'months'=>[self::PAYMENT_METHOD_CODE => $this->getCcMonths()],
                    'years'=>[self::PAYMENT_METHOD_CODE => $this->getCcYears()],
                    'timeoutTime'=>[self::PAYMENT_METHOD_CODE=>$this->getTimeoutTime()],
                    'banks'=>[self::PAYMENT_METHOD_CODE=>$this->getBanks()],
                    'documentTypes'=>[self::PAYMENT_METHOD_CODE=>$this->getDocumentTypes()],
                    'dateCurrent'=>[self::PAYMENT_METHOD_CODE=>$this->getDateCurrent()]
                    //'icons' => $this->getIcons()

                ]

            ]
        ];

        return $config;
    }

    protected function getCvvImageUrl()
    {
        return $this->_ccConfig->getCvvImageUrl();
    }

    protected function getCcAvailableTypes()
    {
        return array("VI"=>"Visa","MC"=>"MasterCard");
        return $this->_ccConfig->getCcAvailableTypes();
    }

  
    protected function getCcMonths()
    {
        //return $this->ccConfig->getCcMonths();
        return array(1 => '01 - Enero',
            2 => '02 - Febrero',
            3 => '03 - Marzo',
            4 => '04 - Abril',
            5 => '05 - Mayo',
            6 => '06 - Junio',
            7 => '07 - Julio',
            8 => '08 - Agosto',
            9 => '09 - Septiembre',
            10 => '10 - Octubre',
            11 => '11 - Noviembre',
            12 => '12 - Diciembre');
    }


    protected function getCcYears()
    {
        return $this->_ccConfig->getCcYears();
        $years = [];
        $first = (int)$this->_date->date('Y');
        for ($index = 0; $index <= 10; $index++) {
            $year = $first + $index;
            $years[$year] = $year;
        }
        return $years;
    }


     protected function getIcons()
    {
        if (!empty($this->icons)) {
            return $this->icons;
        }

        //$types = $this->_ccConfig->getCcAvailableTypes();//var_dump($types);//die;
        $types = ["VI"=>"Visa","MC"=>"MasterCard"];//var_dump($types);die;
        foreach (array_keys($types) as $code) {
            if (!array_key_exists($code, $this->icons)) {
                $asset = $this->_ccConfig->createAsset('Magento_Payment::images/cc/' . strtolower($code) . '.png');
                $placeholder = $this->assetSource->findSource($asset);
                if ($placeholder) {
                    list($width, $height) = getimagesize($asset->getSourceFile());
                    $this->icons[$code] = [
                        'url' => $asset->getUrl(),
                        'width' => $width,
                        'height' => $height
                    ];
                }
            }
        }
        //var_dump($this->icons); die;

        return $this->icons;
    }


    public function getTimeoutTime(){

        return $this->timeoutTime;
    }

    public function getBanks(){

        return $this->_totalPagoService->getBanksAvailables();
        //return  array('1' => "Banesco" );


    }

    public function getDocumentTypes(){

        return array(array("clave"=>"V","valor"=>"V"),array("clave"=>"E","valor"=>"E"),array("clave"=>"J","valor"=>"J"));
    }

    public function getDateCurrent()
    {
        date_default_timezone_set("America/Caracas");

        return date("d/m/Y");
    }
}
