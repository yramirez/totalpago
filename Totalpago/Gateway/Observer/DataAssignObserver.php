<?php
namespace Totalpago\Gateway\Observer;

use Magento\Framework\DataObject;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Exception\LocalizedException;
use Magento\Payment\Observer\AbstractDataAssignObserver;
use Magento\Quote\Api\Data\PaymentInterface;
use Magento\Payment\Model\InfoInterface;

class DataAssignObserver extends AbstractDataAssignObserver
{
    const BANK_TYPE = 'bank_type';
    const REFERENCE_NUMBER = 'reference_number';
    const REFERENCE_DATE = 'reference_date';
    const DOCUMENT_TYPE = 'document_type';
    const CEDULA_NUMBER = 'cedula_number';
    const FULLNAME = 'fullname';
    const TELEPHONE = 'telephone';
    const EMAIL = 'email';

    /**
     * @var array
     */
    protected $additionalInformationList = [
        self::BANK_TYPE,
        self::REFERENCE_NUMBER,
        self::REFERENCE_DATE,
        self::DOCUMENT_TYPE,
        self::CEDULA_NUMBER,
        self::FULLNAME,
        self::TELEPHONE,
        self::EMAIL
    ];
    /**
     * @param Observer $observer
     * @throws LocalizedException
     */
    public function execute(Observer $observer)
    {
      
        $data = $this->readDataArgument($observer);

        $additionalData = $data->getData(PaymentInterface::KEY_ADDITIONAL_DATA);
        if (!is_array($additionalData)) {
                return;
        }

        $paymentInfo = $this->readPaymentModelArgument($observer);

        foreach ($this->additionalInformationList as $additionalInformationKey) {
            if (isset($additionalData[$additionalInformationKey])) {
                $paymentInfo->setAdditionalInformation(
                    $additionalInformationKey,
                    $additionalData[$additionalInformationKey]
                );
            }
        }
        
    }
}