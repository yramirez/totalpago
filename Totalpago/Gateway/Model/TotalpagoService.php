<?php 
namespace Totalpago\Gateway\Model;

class TotalpagoService{

	const API_URL = "https://totalpago.net/WSPasarela/Service.svc";

	const RESOURCE_PAY = "/payment";

	const RESOURCE_BANKS = "/WM_fcn_CuentasBancariasPost";

	const RESOURCE_REGISTER_PAY = "/WM_fcn_RegistrarPagoTransf";

	const RESOURCE_VERIFY_PAY = "/WM_fcn_VerificacionPagoPost ";

	const METHOD_POST = "POST";

	const METHOD_DELETE = "GET";

	private $KeyId;
	private $PublicKeyId;
	private $StatusId=2;


	public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,\Magento\Framework\Encryption\EncryptorInterface $encryptor){

		$this->_scopeConfig = $scopeConfig;
		$this->_encryptor = $encryptor;
	}

	private function getUserId(){
		

		return $this->_encryptor->decrypt($this->_scopeConfig->getValue('payment/totalpago/user_id', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
	}

	private function getToken(){
		
		return $this->_encryptor->decrypt($this->_scopeConfig->getValue('payment/totalpago/token', \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
	}

	public function pay(array $params){

		
		$params["idusr"]= $this->getUserId();
		$params["token"]= $this->getToken();

		$response = json_encode(array("Mensaje"=>"PAGO_INGRESADO"));
		try{
			
			$response = $this->createRequest($params,self::RESOURCE_REGISTER_PAY,self::METHOD_POST);
			return json_decode($response);
		}catch(\Exception $e){

			return json_decode($response);
		}
		

		
	}

	public function verify(array $params){

		
		$params["idusr"]= $this->getUserId();
		$params["token"]= $this->getToken();
		$response = json_encode(array("Mensaje"=>"APROBADO"));
		try{
		$response = $this->createRequest($params,self::RESOURCE_VERIFY_PAY,self::METHOD_POST);

			return json_decode($response);
		}catch(\Exception $e){
			return json_decode($response);
		}
	}

	public function getBanksAvailables(){
		$params["idusr"]= $this->getUserId();
		$params["token"]= $this->getToken();

		try{
			$response = $this->createRequest($params,self::RESOURCE_BANKS,self::METHOD_POST);
		}catch(\Exception $e){
			$response = json_encode(array(
									array("idbancoTransf"=>2,"nuCuentaBancoTransf"=>"013402142122145008","nmbancoTransf"=>"Banesco","rifTitularCuentaTransf"=>"J-312948469","nmTitularCuentaTransf"=>"Totalpago"),
									array("idbancoTransf"=>3,"nuCuentaBancoTransf"=>"01020501810000060354","nmbancoTransf"=>"Banco de Venezuela","rifTitularCuentaTransf"=>"J-312948469","nmTitularCuentaTransf"=>"Totalpago")


		));
		}

		return json_decode($response);
	}

	public function complete(array $params){

	}

	public function cancel(array $params){


	}

	private function createRequest(array $params, $resource,$method){

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,self::API_URL.$resource);
		$data_string = json_encode($params);

		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,30);
		curl_setopt($ch, CURLOPT_TIMEOUT,45);

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST,$method);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','Content-Length: ' . strlen($data_string)));
		$result = curl_exec ($ch);

		if(curl_errno($ch)){
			//return;
			throw new \Exception("Error Processing Request Totalpago", 1);
			
		}

		
		curl_close ($ch);
		return $result;
	}

	
}