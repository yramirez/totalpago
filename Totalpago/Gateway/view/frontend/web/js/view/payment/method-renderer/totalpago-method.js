/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*browser:true*/
/*global define*/
define(
    [
        'jquery',         
        'Totalpago_Gateway/js/view/payment/cc-form',
        'mage/validation',
        'Magento_Checkout/js/model/quote',
        'Magento_Checkout/js/model/full-screen-loader',
        'Magento_Checkout/js/action/set-payment-information',
        'Magento_Checkout/js/action/place-order',
        'Magento_Paypal/js/action/set-payment-method',
        'sweetalert'
    ],
    function ($,Component, validation,quote, fullScreenLoader, setPaymentInformationAction, placeOrder,setPaymentMethodAction,Swal) {
        'use strict';

        return Component.extend({
            initialize: function () {
                    this._super();


                    return this;
            },

            defaults: {
                template: 'Totalpago_Gateway/payment/totalpago',
                timeoutMessage: 'Sorry, but something went wrong. Please contact the seller.',
                transactionResult: ''
            },

            getCode: function () {
                    return 'totalpago';
            },

            initObservable: function () {
                    this._super()
                            .observe([
                                'bankType',
                                'referenceNumber',
                                'referenceDate',
                                'cedulaNumber',
                                'documentType',
                                'fullname',
                                'telephone',
                                'email'
                            ]);

                    return this;
                },

                getData: function () {


                    var data = {
                        'method': this.getCode(),
                        'additional_data': {
                            'bank_type':this.bankType(),
                            'reference_number': this.referenceNumber(),
                            'reference_date':this.referenceDate(),
                            'document_type':this.documentType(),
                            'cedula_number': this.cedulaNumber(),
                            'fullname': this.fullname(),
                            'telephone': this.telephone(),
                            'email': this.email()
                        }
                    };

                    return data;


                },

                isActive: function () {
                    return true;
                },

                validate: function () {
                    var $form = $('#' + this.getCode() + '-form');
                    return $form.validation() && $form.validation('isValid');
                },

                validateKey_2: function (data, event) {

                    var regex = new RegExp("^[a-zA-ZñÑ ]+$");
                    if (event.keyCode != 9) {
                        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                        if (!regex.test(key)) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    }

                    return true;

                },

                validateKey_number: function (data, event) {

                    var regex = new RegExp("^[0-9]+$");

                    if ($('#' + this.getCode() + '_holderid').val().length > 8) {

                        var x = $('#' + this.getCode() + '_holderid').val().substring(0,8)
                        $('#' + this.getCode() + '_holderid').val(x);

                    }else{
                        return true;
                    }


                    if (event.keyCode != 9) {
                        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                        if (!regex.test(key)) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    }

                    return true;
                },

                validateKey_number2: function (data, event) {

                    var regex = new RegExp("^[0-9]+$");

                    if ($('#' + this.getCode() + '_cc_number').val().length > 16) {
                         var x = $('#' + this.getCode() + '_cc_number').val().substring(0,16)
                        $('#' + this.getCode() + '_cc_number').val(x);
                        $('#' + this.getCode() + '_cc_number').focus();

                    }else{
                        return true;
                    }


                    if (event.keyCode != 9) {
                        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                        if (!regex.test(key)) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    }

                    return true;
                },

                validateKey_mes: function (data) {
                    var currentTime = new Date()

// returns the month (from 0 to 11)
                    var month = currentTime.getMonth() + 1

// returns the day of the month (from 1 to 31)
                    var day = currentTime.getDate()

// returns the year (four digits)
                    var year = currentTime.getFullYear()

                    if ($('#' + this.getCode() + '_expiration_yr').val() == year) {
                        if ($('#' + this.getCode() + '_expiration').val() <= month) {
                            $('#' + this.getCode() + '_expiration').prop('selectedIndex', 0);
                        }
                    }
                   

                },

                validateKey_number3: function (data, event) {

                    var regex = new RegExp("^[0-9]+$");

                    if ($('#' + this.getCode() + '_cc_cid').val().length > 3) {
                        var x = $('#' + this.getCode() + '_cc_cid').val().substring(0,3)
                        $('#' + this.getCode() + '_cc_cid').val(x);

                    }else{
                        return true;
                    }


                    if (event.keyCode != 9) {
                        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
                        if (!regex.test(key)) {
                            if (event.keyCode != 8) {
                                event.preventDefault();
                                return false;
                            }
                        }
                    }

                    return true;
                },

                printInfoBank: function (data, event){
                    var bancoId = $('#'+this.getCode()+'_bank_type').val();
                    var banks = this.getBanks();

                     $('#title_tp').html('');
                        $('#nmbancoTransf_text').html('');
                        $('#nmTitularCuentaTransf_text').html('');
                        $('#nuCuentaBancoTransf_text').html('');
                        $('#rifTitularCuentaTransf_text').html('');
                    for(var i=0;i<banks.length;i++){
                     

                        var bank = banks[i];
                       
                         if(bank.idbancoTransf==bancoId){

                            $('#title_tp').html('<b>Información Bancaria:</b>');
                            $('#nmbancoTransf_text').html('<b>Banco: </b>'+bank.nmbancoTransf);
                            $('#nmTitularCuentaTransf_text').html('<b>Razón Social: </b>'+bank.nmTitularCuentaTransf);
                            $('#nuCuentaBancoTransf_text').html('<b># Cuenta: </b>'+bank.nuCuentaBancoTransf);
                            $('#rifTitularCuentaTransf_text').html('<b>RIF: </b>'+bank.rifTitularCuentaTransf);
                        }

                    }
                    

                },
                placeOrder: function (data,event) {

                var self = this;
                if(event){
                    event.preventDefault();
                }

                if(this.validate()){

                    this.isPlaceOrderActionAllowed(false);

                    this.getPlaceOrderDeferredObject()
                    .fail(this.isPlaceOrderActionAllowed(true))
                    .done(function(){
                        if(self.redirectAfterPlaceOrder){
                            $.mage.redirect(window.checkoutConfig.payment.totalpago.checkout.redirect);
                        }
                    });
                }

                return true;
                
               
            },

        });
    }
);
