<?php

namespace Totalpago\Gateway\Helper;

class Data extends \Magento\Framework\App\Helper\AbstractHelper
{


  protected $_tax = "000000000000";

  protected $_uuid;

  protected $_checkoutSession;


  protected $_scopeConfig;

  protected $_urlInterface;


  public function __construct(\Magento\Checkout\Model\Session $checkoutSession,
    \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
    \Magento\Framework\UrlInterface $urlInterface){

    $this->_checkoutSession = $checkoutSession;
    $this->_scopeConfig = $scopeConfig;
    $this->_urlInterface = $urlInterface;

    $this->setTransactionId();
  }

  function getPaymentGatewayUrl() 
  {
    
    return $this->_scopeConfig->getValue("payment/cardnet/submit_url");
  }

  public function getReturnUrl(){

    return $this->_urlInterface->getUrl('cardnet/payment/response');
  }

  public function getErrorMessage($code){

        return $code;

  }

  private function getAllErrorCodes(){

        return array("00"=>"Approved or completed successfully","01"=>"Approved or completed successfully");
  }

  public function setTransactionId()
  {

    $this->_uuid = substr(rand() * 900000 + 100000, 0,6);

  }

  public function getTransactionId()
  {

    return $this->_uuid;

  }


  public function getClientIp(){

     if(isset($_SERVER["REMOTE_ADDR"]) && $_SERVER["REMOTE_ADDR"]=="127.0.0.1")
     {
        return $_SERVER['HTTP_X_FORWARDED_FOR'];
     }else{

        return $_SERVER['REMOTE_ADDR'];
     }
  }


   
}
