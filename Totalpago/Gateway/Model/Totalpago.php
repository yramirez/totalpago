<?php
/**
 * Copyright © 2015 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Totalpago\Gateway\Model;

use Magento\Framework\DataObject;
use Magento\Quote\Api\Data\PaymentInterface;
/**
 * Pay In Store payment method model
 */
class Totalpago extends \Magento\Payment\Model\Method\AbstractMethod
{


    const METHOD_CODE = "totalpago";

    protected $_code = self::METHOD_CODE;

    protected $_isOffline = false ;

    protected $_canAuthorize = true;
    protected $_canCapture = true;
    protected $_isGateway = true;

    public $_cart;


    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Api\ExtensionAttributesFactory $extensionFactory,
        \Magento\Framework\Api\AttributeValueFactory $customAttributeFactory,
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Payment\Model\Method\Logger $logger,
        
        \Magento\Sales\Model\Order\Payment\Transaction\BuilderInterface $transactionBuilder,
        \Magento\Sales\Api\TransactionRepositoryInterface $transactionRepository,
        TotalpagoService $totalpago,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Checkout\Model\Cart $cart,
        array $data = array()
    ) {
        parent::__construct(
            $context,
            $registry,
            $extensionFactory,
            $customAttributeFactory,
            $paymentData,
            $scopeConfig,
            $logger,
            null,
            null,
            $data
        );

       
        $this->_totalpago = $totalpago;

        $this->_transactionBuilder =$transactionBuilder;
        $this->transactionRepository = $transactionRepository;
        $this->messageManager = $messageManager;
        $this->_objectManager = $objectManager;

        $this->_cart = $cart;

        $this->_config = $scopeConfig;

        $this->_logger = $logger;


    }

    public function authorize(\Magento\Payment\Model\InfoInterface $payment, $amount)
    {
        if (!$this->canAuthorize()) {
            throw new \Magento\Framework\Exception\LocalizedException(__('The authorize action is not available.'));
        }


        $order = $payment->getOrder();

        $billing = $order->getBillingAddress();
        $payment->setAmount($amount);
        if ($payment->getParentTransactionId()) {
            $payment->setAnetTransType(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);
            $payment->setXTransId($this->getRealParentTransactionId($payment));
        } else {
            $payment->setAnetTransType(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH);
        }

       $info = $this->getInfoInstance()->getAdditionalInformation();

       $params = array(
                'idPago' => $order->getIncrementId(),
                'mtPago' => $amount,
                'descPago'  => sprintf('#%s, %s', $order->getIncrementId(), $order->getCustomerEmail()),
                'nuReferenciaTransf'=>$info["reference_number"],
                'idbancoTransf'=>$info["bank_type"],
                'fechaTransferencia'=>$info["reference_date"],
                'nacCiTitularCuentaTransferencia'=>$info["document_type"],
                'numeroCiTitularCuentaTransferencia'=>$info["cedula_number"],
                'nmTitularCuentaTransferencia'=>$info["fullname"],
                'telfTitularCuentaTransferencia'=>$info["telephone"],
                'correoTitularCuentaTransferencia'=>$info["email"]

            );

        try{

            $response = $this->_totalpago->pay($params);

            switch ($response->Mensaje) {
                case "PAGO_INGRESADO":
                    header('HTTP/1.0 201 Accepted');
                    /*$transId=substr(rand() * 900000 + 100000, 0,6);
                    $payment->setTransactionId($transId)
                    ->setTransactionAdditionalInfo(
                           \Magento\Sales\Model\Order\Payment\Transaction::RAW_DETAILS,$response->Mensaje
                        );

                    $payment->addTransaction(\Magento\Sales\Model\Order\Payment\Transaction::TYPE_AUTH,null,true);

                    $payment->setIsTransactionClosed(1);*/

                    //$this->createOrderPending($payment, 201);

                    return $this;

                 break;

                 default:

                    header('HTTP/1.0 404 Not Found');
                    throw new \Magento\Framework\Exception\LocalizedException(new \Magento\Framework\Phrase(__('Payment capturing error.').$response->Mensaje));

                 break;
            }


            return $this;

        }catch (\Exception $e){
            header('HTTP/1.0 404 Not Found');
            throw new \Magento\Framework\Exception\LocalizedException(new \Magento\Framework\Phrase(__('Payment capturing error.').$e->getMessage()));
        }

        
    }


    public function createOrderPending($payment,$errorCode){

            switch($errorCode){

                case 201:
                    $errorMsg ="PAGO INGRESADO";
                break;

                case 400:
                    $errorMsg = "Error al validar los datos enviados";
                break;

                case 401:

                    $errorMsg = "Error de autenticación, ha ocurrido un error con las llaves utilizadas.";
                    break;

                case 403:
                     $errorMsg = "Pago Rechazado por el banco.";
                    break;

                case 500:
                     $errorMsg = "Ha Ocurrido un error interno dentro del servidor.";

                case 503:
                     $errorMsg = "Ha Ocurrido un error al procesar los parámetros de entrada. Revise los datos enviados y vuelva a intentarlo.";
                    break;
            }

            $order = $payment->getOrder();
            $orderId=$order->getId();
            $payment->setSkipTransactionCreation(true);

            $state = 'pending_payment';
            $status = 'pending_payment';
            $comment = $errorMsg;
            $isNotified = false;
            $order->setState($state);
            $order->setStatus($status);
            $order->addStatusToHistory($order->getStatus(), $comment);
            $order->save();


            $payment->setIsTransactionPending(true); 
            //$payment->setIsFraudDetected(true);

            //$this->sendEmail($order,self::PATH_EMAIL_TEMPLATE_FAILED);
    }
    protected function getRealParentTransactionId($payment)
    {
        $transaction = $this->transactionRepository->getByTransactionId(
            $payment->getParentTransactionId(),
            $payment->getId(),
            $payment->getOrder()->getId()
        );
        return $transaction->getAdditionalInformation('real_transaction_id');
    }

    public function getRedirectCheckoutPaymentUrl()
    {
        return $this->_cart->getQuote()->getStore()->getUrl("totalpago/payment/verify/");
    }

     
}
